(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
var User,
  extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

module.exports = User = (function(superClass) {
  extend(User, superClass);

  User.prototype.url = function() {
    return "http://localhost:8080/user";
  };

  function User(obj) {
    User.__super__.constructor.call(this, obj);
  }

  return User;

})(Backbone.Model);

},{}],2:[function(require,module,exports){
module.exports = React.createClass({
  propTypes: {
    hashPart: React.PropTypes.string.isRequired,
    hash: React.PropTypes.string
  },
  _shouldRender: function() {
    return this.props.hash === this.props.hashPart;
  },
  renderContent: function() {
    if (this._shouldRender()) {
      return React.createElement("div", {
        "className": "container"
      }, this.props.children);
    }
  },
  render: function() {
    return React.createElement("div", null, this.renderContent());
  }
});

},{}],3:[function(require,module,exports){
module.exports = React.createClass({
  render: function() {
    return React.createElement("div", null);
  }
});

},{}],4:[function(require,module,exports){
var User;

User = require('../models/user.coffee');

module.exports = React.createClass({
  componentWillMount: function() {
    var user;
    console.log("fetching");
    user = new User();
    return user.fetch();
  },
  render: function() {
    return React.createElement("div", {
      "className": "panel panel-primary"
    }, React.createElement("div", {
      "className": "panel-heading"
    }, "Login"), React.createElement("div", {
      "className": "panel-body"
    }, React.createElement("div", {
      "id": "err"
    }), React.createElement("form", null, React.createElement("div", {
      "className": "form-group"
    }, React.createElement("label", null, "Username"), React.createElement("input", {
      "type": "text",
      "className": "form-control",
      "id": "user"
    })), React.createElement("div", {
      "className": "form-group"
    }, React.createElement("label", null, "Password"), React.createElement("input", {
      "type": "password",
      "className": "form-control",
      "id": "password"
    })), React.createElement("button", {
      "type": "submit",
      "className": "btn btn-default"
    }, "Submit"))));
  }
});

},{"../models/user.coffee":1}],5:[function(require,module,exports){
var User;

User = require('../models/user.coffee');

module.exports = React.createClass({
  submit: function() {
    var user;
    user = new User({
      username: this.state.username,
      password: this.state.password
    });
    return user.save({}, {
      success: (function(_this) {
        return function(e, response) {
          if (response.success === false) {
            mixpanel.track("User creation error", _this.state);
            return _this.setState({
              error: !response.success,
              errorMsg: response.msg
            });
          } else {
            mixpanel.track("User created");
            mixpanel.people.set({
              username: _this.state.username
            });
            mixpanel.identify(_this.state.username);
            return window.location.hash = "#game";
          }
        };
      })(this)
    });
  },
  handleUserChange: function(e) {
    return this.setState({
      username: e.target.value
    });
  },
  handlePasswordChange: function(e) {
    return this.setState({
      password: sha256(e.target.value)
    });
  },
  renderError: function() {
    var ref, ref1;
    if (((ref = this.state) != null ? ref.error : void 0) === true) {
      return React.createElement("div", {
        "className": "alert alert-danger"
      }, ((ref1 = this.state) != null ? ref1.errorMsg : void 0));
    }
  },
  render: function() {
    return React.createElement("div", {
      "className": "panel panel-default"
    }, React.createElement("div", {
      "className": "panel-heading"
    }, "Register"), React.createElement("div", {
      "className": "panel-body"
    }, this.renderError(), React.createElement("form", null, React.createElement("div", {
      "className": "form-group"
    }, React.createElement("label", null, "Username"), React.createElement("input", {
      "type": "text",
      "className": "form-control",
      "id": "user",
      "onChange": this.handleUserChange
    })), React.createElement("div", {
      "className": "form-group"
    }, React.createElement("label", null, "Password"), React.createElement("input", {
      "type": "password",
      "className": "form-control",
      "id": "password",
      "onChange": this.handlePasswordChange
    })), React.createElement("button", {
      "type": "button",
      "className": "btn btn-default",
      "onClick": this.submit
    }, "Submit"))));
  }
});

},{"../models/user.coffee":1}],6:[function(require,module,exports){
var Game, Login, Navbar, Page, Register, Site;

Navbar = require('./util/navbar.cjsx');

Page = require('./page.cjsx');

Login = require('./pages/login.cjsx');

Register = require('./pages/register.cjsx');

Game = require('./pages/game.cjsx');

Site = React.createClass({
  componentWillMount: function() {
    $(window).on('hashchange', (function(_this) {
      return function() {
        return _this.setState({
          hash: _this.getHash()
        });
      };
    })(this));
    return this.state || (this.state = {
      hash: this.getHash()
    });
  },
  getHash: (function(_this) {
    return function() {
      return window.location.hash.substr(1);
    };
  })(this),
  render: function() {
    var hashValue;
    hashValue = this.state.hash;
    return React.createElement("div", null, React.createElement(Navbar, null), React.createElement(Page, {
      "hash": hashValue,
      "hashPart": "register"
    }, React.createElement(Register, null)), React.createElement(Page, {
      "hash": hashValue,
      "hashPart": "login"
    }, React.createElement(Login, null)), React.createElement(Page, {
      "hash": hashValue,
      "hashPart": "game"
    }, React.createElement(Game, null)));
  }
});

ReactDOM.render(React.createElement(Site, null), document.getElementById("content"));

},{"./page.cjsx":2,"./pages/game.cjsx":3,"./pages/login.cjsx":4,"./pages/register.cjsx":5,"./util/navbar.cjsx":7}],7:[function(require,module,exports){
module.exports = React.createClass({
  renderBasicNav: function() {
    return React.createElement("ul", {
      "className": "nav navbar-nav"
    }, React.createElement("li", null, React.createElement("a", {
      "href": "#login"
    }, "Login")), React.createElement("li", null, React.createElement("a", {
      "href": "#register"
    }, "New User")));
  },
  render: function() {
    return React.createElement("nav", {
      "className": "navbar navbar-default navbar-static-top"
    }, React.createElement("div", {
      "className": "container"
    }, React.createElement("div", {
      "className": "navbar-header"
    }, React.createElement("a", {
      "className": "navbar-brand",
      "href": "#"
    }, "periapsis")), React.createElement("div", {
      "id": "navbar",
      "className": "navbar-collapse collapse"
    }, this.renderBasicNav())));
  }
});

},{}]},{},[6]);
