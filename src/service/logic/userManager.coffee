_ = require('underscore')
UserTable = "Users"
class UserManager
  constructor: (context) ->
    @ctx = context
    @db = context.db
    
    
  setDefaults: (data) ->
    _.defaults(data,{
      science: 100,
      h3: 100,
      iron: 100
    })
  
    
  getUser: (username, cb) ->
    @db.find(UserTable, {username: username}, cb)

  
  createUser: (data, cb) ->
    if data?.password? and data?.username?
      if data.username.length < 3
        cb({
          success: false,
          msg: "Username must be at least 3 characters long."  
        })
      else
        @getUser data.username, (err, result) =>
          if _.isEmpty(result)
            @db.put(UserTable, @setDefaults({
              username: data.username,
              password: data.password
            }), cb)
          else 
            cb({
              success: false,
              msg: "User already exists."
            })        
    else
      cb({
        success: false,
        msg: "Invalid data supplied to create user function."        
      })
      

module.exports = UserManager
