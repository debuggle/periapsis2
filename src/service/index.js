require('coffee-script/register')
var express = require('express'),
    bodyParser = require('body-parser'),
    cors = require('cors');
  
var ctx = require('./context');
var app = express();

// Middleware
app.use(bodyParser.json());
app.use(cors());

// Application level things
var context = require('./context');

// Controllers
require('./routes/user')(app, context);

// Start the app.
app.listen(8080, function() {
  console.log("periapsis server started.");
});
