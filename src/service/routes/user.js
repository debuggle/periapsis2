var UserTable = "Users";
var _ = require('underscore');

module.exports = function(app, context) {
  app.get("/user/:username", function(req, res) {
    context.userManager.getUser(req.params.username, function(err, data) {
      res.send(data);
    });
  });
  
  app.post("/user", function(req, res) {
    // Check if the user exists.
    var body = req.body;
    context.userManager.createUser(body, function(err, data) {
      if (!_.isEmpty(err)) {
        res.send(err);
      } else {
        res.send({success: true});
      }
    });
  });
}
