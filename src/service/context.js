var Dynamo = require('./storage/dynamo');
var UserManager = require('./logic/userManager');

var context = function() {
  this.db = new Dynamo();
  this.userManager = new UserManager(this);
}

module.exports = new context();
