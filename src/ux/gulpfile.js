var gulp = require('gulp'),
    less = require('gulp-less'),
    exec = require('gulp-exec');
    
gulp.task('libs', function() {
  return gulp.src('./libs/**/*.js')
    .pipe(gulp.dest('../../out/ux/libs'));
});

gulp.task('html', function() {
  return gulp.src('./html/**/*.html')
    .pipe(gulp.dest('../../out/ux'));
});

gulp.task('cjsx', function(cb) {
  return gulp.src('./**/**')
    .pipe(exec("browserify -t cjsxify ./webroot/site.cjsx -o ../../out/ux/bundle.js", function() {
      cb();
    }));
});

gulp.task('assets', function() {
  return gulp.src('./assets/**/**')
    .pipe(gulp.dest('../../out/ux/assets'));
});

gulp.task('default', ['cjsx', 'assets', 'libs', 'html'], function() {
  
});
