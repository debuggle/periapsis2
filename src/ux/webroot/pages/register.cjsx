User = require('../models/user.coffee')
module.exports = React.createClass
  submit: ->
    user = new User({
      username: @state.username,
      password: @state.password
    })
    
    # Submit user
    user.save({},{
      success: (e, response) =>
        if response.success == false
          mixpanel.track("User creation error", @state)
          @setState({
            error: !response.success
            errorMsg: response.msg
          })  
        else
          # Good, navigate to the game page.
          mixpanel.track("User created")
          mixpanel.people.set({ username: @state.username })
          mixpanel.identify(@state.username)
          
          # Navigate
          window.location.hash = "#game"
    })
    
  
  handleUserChange: (e) ->
    @setState({
      username: e.target.value
    })
    
    
  handlePasswordChange: (e) ->
    @setState({
      password: sha256(e.target.value)
    })
    
  
  renderError: ->
    if @state?.error == true
      <div className="alert alert-danger">
        {@state?.errorMsg}
      </div>
    
    
  render: ->
    <div className="panel panel-default">
      <div className="panel-heading">Register</div>
      <div className="panel-body">
        {@renderError()}
        <form>
          <div className="form-group">
            <label>Username</label>
            <input type="text" className="form-control" id="user" onChange={@handleUserChange} />
          </div>
          <div className="form-group">
            <label>Password</label>
            <input type="password" className="form-control" id="password" onChange={@handlePasswordChange} />
          </div>
          <button type="button" className="btn btn-default" onClick={@submit}>Submit</button>
        </form>
      </div>
    </div>
