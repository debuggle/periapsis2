User = require('../models/user.coffee')
module.exports = React.createClass
  componentWillMount: ->
    console.log "fetching"
    user = new User()
    user.fetch()    
    
  render: ->
    <div className="panel panel-primary">
      <div className="panel-heading">Login</div>
      <div className="panel-body">
        <div id="err"></div>
        <form>
          <div className="form-group">
            <label>Username</label>
            <input type="text" className="form-control" id="user" />
          </div>
          <div className="form-group">
            <label>Password</label>
            <input type="password" className="form-control" id="password" />
          </div>
          <button type="submit" className="btn btn-default">Submit</button>
        </form>
      </div>
    </div>
