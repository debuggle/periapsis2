module.exports = React.createClass
  propTypes:
    # The hash that is used to define which hashtag should trigger this page.
    hashPart: React.PropTypes.string.isRequired
    # The actual hash that is being registerd. This is passed in as a prop
    # to allow for manipulation at higher levels.
    hash: React.PropTypes.string
    
    
  _shouldRender: ->
    return @props.hash == @props.hashPart
    
  
  renderContent: ->
    if @_shouldRender()
      <div className="container">
        {@props.children}
      </div>
    
    
  render: ->
    <div>
      {@renderContent()}      
    </div>
