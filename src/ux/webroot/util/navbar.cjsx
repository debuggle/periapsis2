module.exports = React.createClass

  renderBasicNav: ->
    <ul className="nav navbar-nav">
      <li><a href="#login">Login</a></li>
      <li><a href="#register">New User</a></li>
    </ul>

  render: ->
    <nav className="navbar navbar-default navbar-static-top">
      <div className="container">
        <div className="navbar-header">
          <a className="navbar-brand" href="#">periapsis</a>
        </div>
        <div id="navbar" className="navbar-collapse collapse">
          {@renderBasicNav()}
        </div>
      </div>
    </nav>
