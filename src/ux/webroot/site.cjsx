Navbar = require('./util/navbar.cjsx')
Page = require('./page.cjsx')
Login = require('./pages/login.cjsx')
Register = require('./pages/register.cjsx')
Game = require('./pages/game.cjsx')

Site = React.createClass  
  componentWillMount: ->
    # Yuck, but necessary.
    $(window).on('hashchange', => @setState({ hash: @getHash() }))
    @state ||= {
      hash: @getHash()
    }
    
    
  getHash: =>
    return window.location.hash.substr(1);
    
  render: ->
    hashValue = @state.hash
    <div>
      <Navbar />
      <Page hash={hashValue} hashPart="register">
        <Register />
      </Page>
      <Page hash={hashValue} hashPart="login">
        <Login />
      </Page>
      <Page hash={hashValue} hashPart="game">
        <Game />
      </Page>
    </div>

ReactDOM.render(<Site />, document.getElementById("content"));
